opam-version: "2.0"
version: "dev"
maintainer: "Pierre Roux <pierre.roux@onera.fr>"

homepage: "https://prosa.mpi-sws.org/"
dev-repo: "git+https://gitlab.mpi-sws.org/RT-PROOFS/rt-proofs.git"
bug-reports: "https://gitlab.mpi-sws.org/RT-PROOFS/rt-proofs/issues"
license: "BSD-2-Clause"

build: [
  [make "-j%{jobs}%"]
]
install: [make "install"]
depends: [
  "coq" {((>= "8.19" & < "8.21~") | = "dev")}
  "coq-mathcomp-algebra" {((>= "2.2" & < "2.4~") | = "dev")}
  "coq-mathcomp-zify" {>= "1.2.0"}
]

tags: [
  "keyword:prosa"
  "keyword:real-time"
  "keyword:schedulability analysis"
  "keyword:response-time analysis"
  "logpath:prosa"
]
authors: [
  "Felipe Cerqueira"
  "Björn Brandenburg"
  "Maxime Lesourd"
  "Sergey Bozhko"
  "Xiaojie Guo"
  "Sophie Quinton"
  "Marco Maida"
  "Kimaya Bedarkar"
  "Pierre Roux"
]
synopsis: "A Foundation for Formally Proven Schedulability Analysis"
description: """Prosa is a repository of definitions and proofs for
real-time schedulability analysis built with Coq. Prosa’s
distinguishing characteristic is that Prosa prioritizes readability
over all other concerns to ensure that specifications remain
accessible to readers without a background in formal proofs. (A
background in real-time scheduling is assumed.)"""
url {
  src: "git+https://gitlab.mpi-sws.org/RT-PROOFS/rt-proofs.git"
}
